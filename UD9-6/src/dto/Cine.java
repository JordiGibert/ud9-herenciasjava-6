package dto;

public class Cine {

	//crear atributos
	private Pelicula pelicula;
	private double precio;
	private Asiento[][] asientos;
	
	//constructar por defecto
	public Cine() {
		this.asientos = new Asiento[8][9];
		this.asientos=crearVector(asientos);
   	 	this.precio=6.5;
   	 	this.pelicula= new Pelicula();
   	 	llenarSillasR();
    
}
	//Constructor con datos
	 public Cine(int filas, int columnas, double precio, Pelicula pelicula) {
		 asientos = new Asiento[filas][columnas];
		 asientos=crearVector(asientos);
    	 this.precio=precio;
    	 this.pelicula=pelicula;
    	 llenarSillasR();
     
}
	 //Metodo para llenar el vector creado en el constructor de asientos
	 private Asiento[][] crearVector(Asiento[][] asientos) {
		 for (int i = 0; i < asientos.length; i++) {
	    	 for (int j = 0; j < asientos[i].length; j++) {
	    		 Asiento silla = new Asiento();
	    		 asientos[i][j]=silla; 
	    		 String posicio= (i+1)+conseguirLletra(j);
	    		 asientos[i][j].setPosicio(posicio);
	    	 }
		 }
		 return asientos;
	 }
	 
	 //metodo para imprimir por pantalla la matriz creada en el metodo anterior
	 public void escupirVector() {
		 String imprimir= "";
		 for (int i = this.asientos.length-1; i >= 0; i--) {
	    	 for (int j = 0; j < this.asientos[i].length; j++) {
	    		 imprimir= imprimir+this.asientos[i][j].getPosicio()+" ";
	    		 if(this.asientos[i][j].isOcupado()) {
	    			 imprimir=imprimir+"ocupado ";
	    		 } else {
	    			 imprimir=imprimir+"libre   ";
	    		 }
	    	 }
	    	 imprimir=imprimir+"\n";
		 }
		 System.out.println(imprimir);
	 }
	 
	 //Metodo para assignar una letra a partir de un numero (usado para assignar la ubicacion del asiento)
	 private static String conseguirLletra(int i) {
		    char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		    if (i > 25) {
		        return null;
		    }
		    return Character.toString(alphabet[i]);
		}
	 
	 //metodo para assignar los asientos aleatoriamente (50% de posibilidades) como ocupados (pero sin espectadores assignados)
	 private void llenarSillasR() {
		 for (int i = 0; i < this.asientos.length; i++) {
	    	 for (int j = 0; j < this.asientos[i].length; j++) {
	    		 int bool =(int) Math.floor(Math.random()*2);
	    		 if(this.asientos[i][j].isOcupado()==false && bool==0) {
	    			 this. asientos[i][j].llenarSilla(); 
	    		 }
	    	 }
	    	 }
	 }
	 
	 //Metodo para buscar un asiento aleatorio si el espectador puede entrar al cine
	 public void buscarAsiento(Espectador a) {
		 if(entrarCine(a)==true) {
			 boolean sentat=false;
			 while(sentat==false) {
				 int x= (int) Math.floor(Math.random()*(asientos.length));
				 int y = (int) Math.floor(Math.random()*(asientos[x].length));
				 if(asientos[x][y].isOcupado()==false) {
					 sentat=true;
					 asientos[x][y].sentar(a);
					 System.out.println(a.getNombre()+" se a sentado en la posicion "+asientos[x][y].getPosicio());
				 }
			 }
			
		 } else {
			 System.out.println("No es posible entrar a la sala de cine");
		 }
	 }
	 
	 //metodo que comprueva si el espectador cumple los requisistos para entrar en el cine
	 private boolean entrarCine(Espectador a) {
		 boolean entra=false;
		 if(haySitio() ==true && a.tieneEdad(pelicula.getEdadMinima())) {
			 if(a.tieneDinero(precio)) {
				 entra=true;
			 }
		 }
		 return entra;
	 }
	
	 //metodo que comprueva si hay un sitio libre en el cine
	 public boolean haySitio() {
		 boolean ocupado = false;

         for (int i = 0; i < asientos.length; i++) {
             for (int j = 0; j < asientos[i].length; j++) {

                 if (asientos[i][j].isOcupado()) {
                    ocupado = true;
                 }

             }
         }

         return ocupado;
     }
}
