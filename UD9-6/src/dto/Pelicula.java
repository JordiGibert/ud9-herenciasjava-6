package dto;

public class Pelicula {
	private String titulo;
    private int duracion;
    private int edadMinima;
    private String director;
 
    //Constructor/
    public Pelicula() {
        this.titulo = "";
        this.duracion = 120;
        this.edadMinima = 12;
        this.director = "";
    }
    
    public Pelicula(String titulo, int duracion, int edadMinima, String director) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.edadMinima = edadMinima;
        this.director = director;
    }
 

    public String getTitulo() {
        return titulo;
    }
 
 
    public int getDuracion() {
        return duracion;
    }

 
    public int getEdadMinima() {
        return edadMinima;
    }
 
    public String getDirector() {
        return director;
    }
 

    public String toString() {
        return "Pelicula : " + titulo + "' del director " + director + ", tiene una duracion de " + duracion + " minutos y la edad minima es de " + edadMinima + " a�os";
    }

}
