package dto;

public class Espectador {
	//crear atyributos
	private String nombre;
	private int edad;
	private double dinero;
	//cponstructor (solo lleno)
	public Espectador(String nombre, int edad, double dinero) {
		this.nombre = nombre;
		this.edad = edad;
		this.dinero = dinero;
	}
	//getters i seters
	public String getNombre() {
		return nombre;
	}
	public int getEdad() {
		return edad;
	}
	public double getDinero() {
		return dinero;
	}
	
	//metodo para comprovar si la edad de la pelicula es mayor o menor a la del espectador
	public boolean tieneEdad(int edadMin) {
		boolean permitido = false;
		if(edad>=edadMin) {
			permitido=true;
		} 
		return permitido;
	}
	
	//metodo que comprueva que el espectador pueda pagar la entrada y le resta el dinero
	public boolean tieneDinero(double precio) {
		boolean entrada=false;
		if(dinero>=precio) {
			dinero=dinero-precio;
			entrada=true;
		}
		return entrada;
	}
	
}
