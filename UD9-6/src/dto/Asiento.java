package dto;

public class Asiento {
	
	//Crear atributos
	private boolean ocupado;
	private Espectador espectador;
	private String posicio;
	
	//Constructor vacio
	public Asiento() {
		this.ocupado = false;
		this.espectador= null;
		this.posicio= null;
	}
	
	//metodos 
	public boolean isOcupado() {
		return ocupado;
	}
	
	public void llenarSilla() {
		this.ocupado=true;
	}
	public Espectador getEspectador() {
		return espectador;
	}
	public void sentar(Espectador nuevo) {
		this.ocupado = true;
		this.espectador=nuevo;
	}
	
	//getters i setters
	public String getPosicio() {
		return posicio;
	}
	public void setPosicio(String posicio) {
		this.posicio = posicio;
	}
	
	
}
