import dto.*;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//crear pelicula con los datos por defecto
		Pelicula pel1=new Pelicula();
		//Crear cine con los datos por defecto
		Cine cin1=new Cine();
		//Crear 6 espectadores con los datos por defecto
		Espectador a= new Espectador("Pepe",8,13.0);
		Espectador b= new Espectador("Juan",20,9.0);
		Espectador c= new Espectador("Laura",15,5.0);
		Espectador d= new Espectador("Maria",15,12.0);
		Espectador e= new Espectador("Paca",14,15.0);
		Espectador f= new Espectador("Paco",55,13.0);
		
		//Mostrar matriz del cine
		cin1.escupirVector();
		
		//Intentar sentar a los 6 espectadores
		cin1.buscarAsiento(a);
		cin1.buscarAsiento(b);
		cin1.buscarAsiento(c);
		cin1.buscarAsiento(d);
		cin1.buscarAsiento(e);
		cin1.buscarAsiento(f);
		
		System.out.println();
		// Mostrar de nuevo matriz para ver canvios
		cin1.escupirVector();
	}
	 

}
